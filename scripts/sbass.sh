#!/bin/bash

(cat $1 | /export/home1/NoCsBack/dtai/jodv/fo-symmetries/binaries/sbass/sbass --stats 3>&1 1>&2- 2>&3- ) 2> $1.brk | awk '
/nodes/ {print $3}
/edges/ {print $3}
/generators/ {print $3}
/add. rules/ {print $4}
/add. atoms/ {print $4}
' | tr '\012' ','
