On Local Domain Symmetry for Model Expansion: experiments
========
This repository contains necessary information to rerun the experiments in the paper "On Local Domain Symmetry for Model Expansion".

Firstly, all problem files can be found in the map "[instances](https://bitbucket.org/krr/fo-sym-experiments/src/988a016e7e739b7f5fc0833e5da36903bb8fc7d0/instances/?at=master)". It contains both theory and instance files for both ASP and FO(.) specifications of the respective problems.

Secondly, all raw data from our experiment can be found in the map "[raw data](https://bitbucket.org/krr/fo-sym-experiments/src/988a016e7e739b7f5fc0833e5da36903bb8fc7d0/raw_data/?at=master)". These csv files contain i.a. running times and memory usage for each step in the solving process of the different configurations. In particular, "[syminfo.csv](https://bitbucket.org/krr/fo-sym-experiments/src/988a016e7e739b7f5fc0833e5da36903bb8fc7d0/raw_data/syminfo.csv?at=master&fileviewer=file-view-default)" contains information on IDP's symmetry detection step.

Thirdly, "[summary.ods](https://bitbucket.org/krr/fo-sym-experiments/src/988a016e7e739b7f5fc0833e5da36903bb8fc7d0/summary.ods?at=master&fileviewer=file-view-default)" summarizes the raw data into an overview for the 4 compared solving runs. Make sure to open the extra tabs in the spreadsheet.

Fourthly, the folder "[scripts](https://bitbucket.org/krr/fo-sym-experiments/src/988a016e7e739b7f5fc0833e5da36903bb8fc7d0/scripts/?at=master)" contains the core scripts that called the corresponding binaries. These scripts are not runnable on their own; they were part of a local [Benchie](https://bitbucket.org/krr/benchie) configuration. Nonetheless, they contain information on how our benchmarks were executed.

Lastly, the folder "[idp](https://bitbucket.org/krr/fo-sym-experiments/src/6025a7cfec48b9f29c7d44290d0d0101510742d3/idp/?at=master)" contains both binaries and source code of the exact version of IDP used in this experiment.