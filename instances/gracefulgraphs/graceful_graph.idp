/**
*    Graceful Graphs
*/

include <table_utils>

vocabulary FactListVoc{
    type node
    type val isa nat
    edge(node,node)
}

vocabulary V{
    extern vocabulary FactListVoc
    edge_value(node,node,val)
    value(node,val)
    Edge(node,node)
    Difference(val,val):val
}

theory T:V{
    { !x y: Edge(x,y) <- edge(x,y) | edge(y,x). }
    { !x y z: Difference(x,y)=z <- z=abs(x-y). }

		// all-different on node values
    !x y z: value(x,z) & value(y,z) => x=y.
    // value forms a function
    !x: ?1y: value(x,y).
		// value is symmetric
		!x y z: edge_value(x,y,z) => edge_value(y,x,z).
		
		// edge_value forms a partial function 
		!x y: Edge(x,y) => ?1 z: edge_value(x,y,z).
		// edge_value is not defined for non-edges
		!x y z: ~Edge(x,y) => ~edge_value(x,y,z).

		// edge_value constraint
    !x y a b: Edge(x,y) & value(x,a) & value(y,b) => edge_value(x,y,Difference(a,b)).

		// all-different on edge_value
    !x1 y1 x2 y2 z: Edge(x1,y1) & Edge(x2,y2) & edge_value(x1,y1,z) & edge_value(x2,y2,z) => (x1=x2 & y1=y2) | (x1=y2 & y1=x2).
}

procedure setOptions(){
//    stdoptions.symmetrybreaking="static"
    stdoptions.verbosity.symmetrybreaking=2
    stdoptions.verbosity.grounding=1
    stdoptions.verbosity.solving=1
}

procedure getStructure(){
    local nb = #totable(S[FactListVoc::edge].ct)
    S[V::val.type]=range(0,nb)
    setvocabulary(S,V)
    return S
}

procedure getTheory(){
    return T
}

procedure main(){
	setOptions()
  local sol = modelexpand(getTheory(),getStructure())
	io.write("Number of models: ")
	print(#sol)
//  print(sol[1])
}

structure Simple:V{
    val={0..2}
    edge={a,b;b,c;}
}
